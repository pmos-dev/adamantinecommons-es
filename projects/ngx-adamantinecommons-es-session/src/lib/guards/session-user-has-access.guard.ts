import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';

import { CommonsAdamantineManagedFirstClassUserModelSessionRestClientService } from 'tscommons-es-rest-adamantine';
import { ICommonsAdamantineManagedFirstClassUser } from 'tscommons-es-models-adamantine';
import { ECommonsAdamantineAccess, computeECommonsAdamantineAccess } from 'tscommons-es-models-adamantine';

export abstract class AdamantineSessionUserHasAccessGuard<
		M extends ICommonsAdamantineManagedFirstClassUser
> implements CanActivate {
	constructor(
			private restService: CommonsAdamantineManagedFirstClassUserModelSessionRestClientService<M>,
			private requiredAccess: ECommonsAdamantineAccess
	) {}
	
	async canActivate(
			_next: ActivatedRouteSnapshot,
			_state: RouterStateSnapshot
	): Promise<boolean> {
		// We assume that the session guard has already been run
		
		const sessionUser: M = await this.restService.getSessionUser();
		
		return computeECommonsAdamantineAccess(
				sessionUser.access,
				this.requiredAccess
		);
	}
}

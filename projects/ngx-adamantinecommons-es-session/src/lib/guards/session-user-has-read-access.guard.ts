import { CommonsAdamantineManagedFirstClassUserModelSessionRestClientService } from 'tscommons-es-rest-adamantine';
import { ICommonsAdamantineManagedFirstClassUser } from 'tscommons-es-models-adamantine';
import { ECommonsAdamantineAccess } from 'tscommons-es-models-adamantine';

import { AdamantineSessionUserHasAccessGuard } from './session-user-has-access.guard';

export abstract class AdamantineSessionUserHasReadAccessGuard<
		M extends ICommonsAdamantineManagedFirstClassUser
> extends AdamantineSessionUserHasAccessGuard<M> {
	constructor(
			restService: CommonsAdamantineManagedFirstClassUserModelSessionRestClientService<M>
	) {
		super(
				restService,
				ECommonsAdamantineAccess.READ
		);
	}
}

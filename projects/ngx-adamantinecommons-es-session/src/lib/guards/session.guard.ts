import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';

import { CommonsAdamantineManagedFirstClassUserModelSessionRestClientService } from 'tscommons-es-rest-adamantine';
import { ICommonsAdamantineManagedFirstClassUser } from 'tscommons-es-models-adamantine';

// No real advantage to having a secondclass implementation, as its controlled by the back end and is sort of implicit if you supply a second class as M

export abstract class AdamantineSessionGuard<
		M extends ICommonsAdamantineManagedFirstClassUser
> implements CanActivate {
	constructor(
			private restService: CommonsAdamantineManagedFirstClassUserModelSessionRestClientService<M>,
			private failCallback?: () => void
	) {}
	
	async canActivate(
			_next: ActivatedRouteSnapshot,
			_state: RouterStateSnapshot
	): Promise<boolean> {
		const validated: boolean = await this.restService.validate();
		if (validated) return true;
		
		if (this.failCallback) this.failCallback();

		return false;
	}
}

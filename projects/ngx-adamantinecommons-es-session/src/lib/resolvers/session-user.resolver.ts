import { Resolve, RouterStateSnapshot, ActivatedRouteSnapshot } from '@angular/router';

import { CommonsAdamantineManagedFirstClassUserModelSessionRestClientService } from 'tscommons-es-rest-adamantine';
import { ICommonsAdamantineManagedFirstClassUser } from 'tscommons-es-models-adamantine';

// No real advantage to having a secondclass implementation, as its controlled by the back end and is sort of implicit if you supply a second class as M

export abstract class AdamantineSessionUserResolver<
		M extends ICommonsAdamantineManagedFirstClassUser
> implements Resolve<M> {
	
	constructor(
			private restService: CommonsAdamantineManagedFirstClassUserModelSessionRestClientService<M>
	) {}

	public resolve(_route: ActivatedRouteSnapshot, _state: RouterStateSnapshot): Promise<M> {
		return this.restService.getSessionUser();
	}

}

/*
 * Public API Surface of ngx-adamantinecommons-managed
 */

export * from './lib/components/data-fields-form/data-fields-form.component';
export * from './lib/components/m2m-model-editor/m2m-model-editor.component';
export * from './lib/components/m2m-model-grid-editor/m2m-model-grid-editor.component';
export * from './lib/components/managed-model/managed-model.component';
export * from './lib/components/managed-model-form/managed-model-form.component';
export * from './lib/components/managed-model-list/managed-model-list.component';

// duration is not exported as it is only used internally

export * from './lib/services/outcome-snack.service';

export * from './lib/types/tm2m-link-pair';
export * from './lib/types/tmanaged-model-custom-action';
export * from './lib/types/tmanaged-model-custom-action-event';
export * from './lib/types/tmanaged-model-movement';
export * from './lib/types/tmanaged-model-create-data';

export * from './lib/ngx-adamantinecommons-es-managed.module';

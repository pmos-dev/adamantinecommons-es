import { Component, EventEmitter, OnChanges, Output, SimpleChanges } from '@angular/core';
import { Input } from '@angular/core';

import { commonsDateDateToHi, commonsDateHiToDate } from 'tscommons-es-core';

import { CommonsComponent } from 'ngx-angularcommons-es-core';

type TOption = {
		date: Date;
		pretty: string;
};

const options: TOption[] = [
		commonsDateHiToDate('00:00', true),
		commonsDateHiToDate('00:01', true),
		commonsDateHiToDate('00:02', true),
		commonsDateHiToDate('00:03', true),
		commonsDateHiToDate('00:04', true),
		commonsDateHiToDate('00:05', true),
		commonsDateHiToDate('00:10', true),
		commonsDateHiToDate('00:15', true),
		commonsDateHiToDate('00:20', true),
		commonsDateHiToDate('00:30', true),
		commonsDateHiToDate('00:45', true),
		commonsDateHiToDate('01:00', true),
		commonsDateHiToDate('01:30', true),
		commonsDateHiToDate('02:00', true),
		commonsDateHiToDate('03:00', true),
		commonsDateHiToDate('04:00', true),
		commonsDateHiToDate('05:00', true),
		commonsDateHiToDate('06:00', true),
		commonsDateHiToDate('09:00', true),
		commonsDateHiToDate('12:00', true),
		commonsDateHiToDate('24:00', true)
]
		.map((d: Date): TOption => ({
				date: new Date(d.getTime()),
				pretty: commonsDateDateToHi(d, true)
		}));

// used internally only

@Component({
		selector: 'adamantine-data-fields-form-duration',
		templateUrl: './duration.component.html',
		styleUrls: ['./duration.component.less']
})
export class DurationComponent extends CommonsComponent implements OnChanges {
	@Input() duration: Date|undefined;
	@Output() durationChange: EventEmitter<Date|undefined> = new EventEmitter<Date|undefined>(true);

	@Input() disabled: boolean = false;
	@Input() upwards: boolean|undefined;
	@Input() clearable: boolean = true;
	@Input() placeholder: string|undefined;
	@Input() helper: string|undefined;
	@Input() autoDropdownDirection: boolean = true;
	@Input() ucFieldNames: boolean = false;

	options: string[] = options
			.map((option: TOption): string => option.pretty);
	
	option: string|undefined;

	ngOnChanges(changes: SimpleChanges): void {
		if (!changes.duration) return;
		if (!this.duration) return;

		const pretty: string = commonsDateDateToHi(this.duration, true);

		const match: TOption|undefined = options
				.find((o: TOption): boolean => o.pretty === pretty);
		if (!match) {
			this.option = undefined;
			return;
		}

		this.option = match.pretty;
	}

	doValueChange(): void {
		if (!this.option) {
			this.durationChange.emit(undefined);
			return;
		}

		const match: TOption|undefined = options
				.find((o: TOption): boolean => o.pretty === this.option);
		if (!match) {
			this.option = undefined;
			this.durationChange.emit(undefined);
			return;
		}

		this.durationChange.emit(new Date(match.date.getTime()));
	}
}

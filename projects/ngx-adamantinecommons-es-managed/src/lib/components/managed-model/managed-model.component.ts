import { Component, OnInit, OnChanges, SimpleChanges } from '@angular/core';
import { Input, Output, EventEmitter } from '@angular/core';

import { commonsBase62GenerateRandomId } from 'tscommons-es-core';
import { commonsArrayIntersect } from 'tscommons-es-core';
import { ECommonsMoveDirection } from 'tscommons-es-models';
import { ICommonsManaged, isICommonsManaged } from 'tscommons-es-models-adamantine';
import { ICommonsManagedModelMetadata } from 'tscommons-es-models-adamantine';
import { ECommonsAdamantineAccess, computeECommonsAdamantineAccess } from 'tscommons-es-models-adamantine';

import { CommonsComponent } from 'ngx-angularcommons-es-core';

import { ECommonsTableSelectable } from 'ngx-materialcommons-es-table';
import { CommonsDialogService } from 'ngx-materialcommons-es-app';
import { CommonsDrawerService } from 'ngx-materialcommons-es-mobile';
import { ECommonsDrawerDirection } from 'ngx-materialcommons-es-mobile';

import { TManagedModelMovement } from '../../types/tmanaged-model-movement';
import { TManagedModelCustomAction } from '../../types/tmanaged-model-custom-action';
import { TManagedModelCustomActionEvent } from '../../types/tmanaged-model-custom-action-event';
import { TManagedModelCreateData } from '../../types/tmanaged-model-create-data';

@Component({
		selector: 'adamantine-managed-model',
		templateUrl: './managed-model.component.html',
		styleUrls: ['./managed-model.component.less']
})
export class ManagedModelComponent extends CommonsComponent implements OnInit, OnChanges {
	ECommonsMoveDirection = ECommonsMoveDirection;
	ECommonsTableSelectable = ECommonsTableSelectable;
	ECommonsDrawerDirection = ECommonsDrawerDirection;
	
	@Input() metadata: ICommonsManagedModelMetadata|undefined;
	@Input() rows: ICommonsManaged[] = [];
	@Input() allowMove: boolean = false;
	@Input() paginate: number|undefined;
	@Input() rowActions: boolean = false;
	@Input() customActions: TManagedModelCustomAction[] = [];
	@Input() sessionAccess?: ECommonsAdamantineAccess;
	@Input() autoDropdownDirection: boolean = true;
	@Input() ucFieldNames: boolean = false;
	@Input() columnWidths: { [field: string]: string } = {};
	@Input() readOnlyRows: ICommonsManaged[] = [];
	@Input() disableCustomActionsForReadOnlyRows: boolean = false;
	@Input() drawerEditor: ECommonsDrawerDirection|undefined;

	@Output() create: EventEmitter<TManagedModelCreateData> = new EventEmitter<TManagedModelCreateData>(true);
	@Output() update: EventEmitter<ICommonsManaged> = new EventEmitter<ICommonsManaged>(true);
	@Output() delete: EventEmitter<ICommonsManaged[]> = new EventEmitter<ICommonsManaged[]>(true);
	@Output() move: EventEmitter<TManagedModelMovement> = new EventEmitter<TManagedModelMovement>(true);
	@Output() customAction: EventEmitter<TManagedModelCustomActionEvent> = new EventEmitter<TManagedModelCustomActionEvent>(true);

	selecteds: ICommonsManaged[] = [];
	formData: ICommonsManaged|undefined;
	
	isEditing: boolean = false;
	internalDrawerName: string;
	drawerTitle: string|undefined;
	
	constructor(
			private dialogService: CommonsDialogService,
			private drawerService: CommonsDrawerService
	) {
		super();

		this.internalDrawerName = commonsBase62GenerateRandomId();
	}
	
	ngOnInit(): void {
		
		this.subscribe(
				this.drawerService.cancelledObservable(),
				(id: string): void => {
					if (this.drawerEditor !== undefined && this.internalDrawerName === id) {
						this.isEditing = false;
						this.formData = undefined;
					}
				}
		);
	}
	
	ngOnChanges(changes: SimpleChanges): void {
		if (changes.rows) {
			this.selecteds = [];

			if (this.drawerEditor) {
				if (this.internalDrawerName) this.drawerService.hide(this.internalDrawerName);
				
				setTimeout(
						(): void => {
							// give the drawer time to close before ngIf gets rid of the content
							this.formData = undefined;
							this.isEditing = false;
						},
						500
				);
			} else {
				this.formData = undefined;
				this.isEditing = false;
			}
		}
	}
	
	hasAccessToManage(): boolean {
		if (!this.sessionAccess) return true;
		if (!this.metadata) return false;
		
		return computeECommonsAdamantineAccess(
				this.sessionAccess,
				this.metadata.accessRequiredToManage
		);
	}
	
	hasReadOnlyRow(): boolean {
		if (this.readOnlyRows.length === 0) return false;
		
		return commonsArrayIntersect<ICommonsManaged>(
				this.selecteds,
				this.readOnlyRows,
				isICommonsManaged,
				(
						a: ICommonsManaged,
						b: ICommonsManaged
				): boolean => a.id === b.id
				
		).length > 0;
	}
		
	isFirst(): boolean {
		if (this.rows.length === 0) return false;
		
		for (const row of this.selecteds) {
			if (row.id === this.rows[0].id) return true;
		}
		
		return false;
	}
	
	isLast(): boolean {
		if (this.rows.length === 0) return false;
		
		for (const row of this.selecteds) {
			if (row.id === this.rows[this.rows.length - 1].id) return true;
		}
		
		return false;
	}
	
	doAdd(): void {
		if (this.selecteds.length !== 0) throw new Error('Attempting to add when a row is already selected. This should not be possible.');
		
		this.selecteds = [];
		this.formData = undefined;
		this.isEditing = true;
		
		if (this.drawerEditor && this.internalDrawerName) {
			this.drawerTitle = 'Add new item';
			this.drawerService.show(this.internalDrawerName);
		}
	}
	
	doEdit(row?: ICommonsManaged): void {
		if (row === undefined && this.selecteds.length !== 1) throw new Error('Attempting to edit a non-single row. This should not be possible.');
		
		this.formData = row || this.selecteds[0];
		this.isEditing = true;
		
		if (this.drawerEditor && this.internalDrawerName) {
			this.drawerTitle = 'Edit item details';
			this.drawerService.show(this.internalDrawerName);
		}
	}
	
	doDelete(row?: ICommonsManaged): void {
		if (row === undefined && this.selecteds.length === 0) throw new Error('Attempting to delete zero rows. This should not be possible.');
		
		this.dialogService.deleteConfirm(
				'Delete item',
				`Are you sure you want to delete ${row ? row.name : 'these rows'}`,
				(): void => {
					this.delete.emit(row ? [ row ] : this.selecteds);
				},
				(): void => { /* do nothing */ }
		);
	}
	
	doCreate(formData: TManagedModelCreateData): void {
		this.create.emit(formData);
		
		// the isEditing false and formData undefined are done by the ngOnChanges
	}
	
	doUpdate(row: ICommonsManaged): void {
		this.update.emit(row);

		// the isEditing false and formData undefined should be done by the ngOnChanges.
		// is this necessary?

		this.isEditing = false;
		this.formData = undefined;
	}
	
	doCancel(): void {
		this.isEditing = false;
		this.formData = undefined;
		
		if (this.drawerEditor && this.internalDrawerName) {
			this.drawerService.hide(this.internalDrawerName);
		}
	}
	
	doMove(direction: ECommonsMoveDirection, row?: ICommonsManaged): void {
		if (row === undefined && this.selecteds.length === 0) throw new Error('Attempting to move zero rows. This should not be possible');
		
		const ids: number[] = (row ? [ row ] : this.selecteds)
				.map((r: ICommonsManaged): number => r.id);
		
		// the looping has to be in the same order as the rows for bulk operations
		const ordered: ICommonsManaged[] = this.rows
				.filter((r: ICommonsManaged): boolean => ids.includes(r.id));
		
		this.move.emit({
				selecteds: ordered,
				direction: direction
		});
	}
	
	doCustomAction(action: TManagedModelCustomAction): void {
		if (action.selectable === undefined) {
			if (this.selecteds.length > 0) return;
			
			this.customAction.emit({
					action: action.action,
					selecteds: []
			});
			return;
		}
		
		if (action.selectable === ECommonsTableSelectable.SINGLE) {
			if (this.selecteds.length !== 1) return;
			
			this.customAction.emit({
					action: action.action,
					selecteds: [ this.selecteds[0] ]
			});
			return;
		}
		
		if (action.selectable === ECommonsTableSelectable.MULTIPLE) {
			if (this.selecteds.length === 0) return;
			
			this.customAction.emit({
					action: action.action,
					selecteds: [ ...this.selecteds ]
			});
			return;
		}
	}
}

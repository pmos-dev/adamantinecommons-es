import { Component } from '@angular/core';
import { Input } from '@angular/core';

import { commonsStringUcWords } from 'tscommons-es-core';
import { TPropertyObject } from 'tscommons-es-core';
import { ICommonsManagedModelField } from 'tscommons-es-models-adamantine';
import { ECommonsAdamantineManagedModelFieldType } from 'tscommons-es-models-adamantine';

import { CommonsComponent } from 'ngx-angularcommons-es-core';

@Component({
		selector: 'adamantine-data-fields-form',
		templateUrl: './data-fields-form.component.html',
		styleUrls: ['./data-fields-form.component.less']
})
export class DataFieldsFormComponent extends CommonsComponent {
	ECommonsAdamantineManagedModelFieldType = ECommonsAdamantineManagedModelFieldType;
	
	@Input() dataFields: ICommonsManagedModelField[] = [];
	@Input() data: TPropertyObject = {};
	@Input() disabled: boolean = false;
	@Input() autoDropdownDirection: boolean = true;
	@Input() ucFieldNames: boolean = false;
	
	getPlaceholder(field: ICommonsManagedModelField): string|undefined {
		const optional: string = field.optional ? ' (optional)' : '';
		
		let label: string|undefined = field.description || field.name || '';

		if (this.ucFieldNames) label = commonsStringUcWords(label);
		
		if (optional) label = `${label} (optional)`;
		
		return label;
	}
}

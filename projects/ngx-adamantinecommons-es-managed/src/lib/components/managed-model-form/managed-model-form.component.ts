import { Component, OnInit, OnChanges, SimpleChanges } from '@angular/core';
import { Input, Output, EventEmitter } from '@angular/core';

import {
		commonsTypeHasProperty,
		commonsTypeHasPropertyString,
		commonsTypeAttemptBoolean,
		commonsTypeIsString,
		commonsTypeIsStringArray,
		commonsDateDateToYmdHis,
		commonsDateDateToYmd,
		commonsDateDateToHi,
		commonsDateYmdHisToDate,
		commonsDateYmdToDate,
		commonsDateHiToDate
} from 'tscommons-es-core';
import { TPropertyObject } from 'tscommons-es-core';
import { COMMONS_REGEX_PATTERN_EMAIL } from 'tscommons-es-core';
import { COMMONS_REGEX_PATTERN_URL } from 'tscommons-es-core';
import { ICommonsManaged } from 'tscommons-es-models-adamantine';
import { ICommonsManagedModelMetadata } from 'tscommons-es-models-adamantine';
import { ICommonsManagedModelField } from 'tscommons-es-models-adamantine';
import { ECommonsAdamantineManagedModelFieldType } from 'tscommons-es-models-adamantine';

import { CommonsComponent } from 'ngx-angularcommons-es-core';

import { CommonsSnackService } from 'ngx-materialcommons-es-mobile';

import { TManagedModelCreateData } from '../../types/tmanaged-model-create-data';

type TValidityOutcome = {
		valid: true;
		toSave: TManagedModelCreateData;
} | {
		valid: false;
		invalidField: ICommonsManagedModelField;
};

@Component({
		selector: 'adamantine-managed-model-form',
		templateUrl: './managed-model-form.component.html',
		styleUrls: ['./managed-model-form.component.less']
})
export class ManagedModelFormComponent extends CommonsComponent implements OnInit, OnChanges {
	@Input() metadata: ICommonsManagedModelMetadata|undefined;
	@Input() row: ICommonsManaged|undefined;
	@Input() autoDropdownDirection: boolean = true;
	@Input() ucFieldNames: boolean = false;

	@Output() private create: EventEmitter<TManagedModelCreateData> = new EventEmitter<TManagedModelCreateData>(true);
	@Output() private update: EventEmitter<ICommonsManaged> = new EventEmitter<ICommonsManaged>(true);
	@Output() private cancel: EventEmitter<void> = new EventEmitter<void>(true);

	internalRow: TPropertyObject = {};

	constructor(
			private snackService: CommonsSnackService
	) {
		super();
	}

	ngOnInit(): void {
		super.ngOnInit();
		
		this.load();
	}
	
	ngOnChanges(_changes: SimpleChanges): void {
		this.load();
	}
	
	private load(): void {
		this.internalRow = !this.row ? {} : { ...this.row };
		
		if (this.metadata) {
			for (const field of this.metadata.manageableFields) {
				if (!commonsTypeHasProperty(this.internalRow, field.name)) {
					// silly typescript hack because !commonsTypeHasProperty resolves to 'test not TPropertyObject'
					(this.internalRow as TPropertyObject)[field.name] = undefined;
				} else {
					switch (field.type) {
						case ECommonsAdamantineManagedModelFieldType.DATETIME:
							this.internalRow[field.name] = commonsDateDateToYmdHis(this.internalRow[field.name]).replace(' ', 'T');
							break;
						case ECommonsAdamantineManagedModelFieldType.DATE:
							this.internalRow[field.name] = commonsDateDateToYmd(this.internalRow[field.name]);
							break;
						case ECommonsAdamantineManagedModelFieldType.TIME:
							this.internalRow[field.name] = commonsDateDateToHi(this.internalRow[field.name]);
							break;
						case ECommonsAdamantineManagedModelFieldType.BOOLEAN:
							this.internalRow[field.name] = commonsTypeAttemptBoolean(this.internalRow[field.name]) || false;
							break;
						case ECommonsAdamantineManagedModelFieldType.STRINGARRAY:
							if (commonsTypeIsString(this.internalRow[field.name])) {
								this.internalRow[field.name] = this.internalRow[field.name].trim() === '' ? undefined : this.internalRow[field.name].trim();
							}
							if (commonsTypeIsStringArray(this.internalRow[field.name])) {
								this.internalRow[field.name] = this.internalRow[field.name]
										.map((item: string): string => item.trim())
										.filter((item: string): boolean => item !== '')
										.join(',').trim();
								if (this.internalRow[field.name] === '') {
									this.internalRow[field.name] = undefined;
								}
							}
							break;
					}
				}
			}
		}
	}
	
	private validateForm(): TValidityOutcome {
		if (!this.metadata) {
			return {
					valid: false,
					invalidField: undefined as any	// hack
			};
		}
		
		const toSave: TPropertyObject = { ...this.internalRow };

		let invalidField: ICommonsManagedModelField|undefined;
		
		for (const field of this.metadata.manageableFields) {
			if (field.type === 'boolean') toSave[field.name] = commonsTypeAttemptBoolean(toSave[field.name]) || false;
			
			if (
				toSave[field.name] === undefined
				|| (commonsTypeIsString(toSave[field.name]) && (toSave[field.name] as string).trim() === '')
			) {
				toSave[field.name] = undefined;
				
				if (!field.optional) {
					invalidField = field;
					break;
				}
			} else {
				try {
					switch (field.type) {
						case ECommonsAdamantineManagedModelFieldType.DATETIME:
							let temp: string = toSave[field.name].replace('T', ' ');
							if (!/[0-9]{2}:[0-9]{2}:[0-9]{2}$/.test(temp)) temp = `${temp}:00`;
							toSave[field.name] = commonsDateYmdHisToDate(temp);
							break;
						case ECommonsAdamantineManagedModelFieldType.DATE:
							toSave[field.name] = commonsDateYmdToDate(toSave[field.name]);
							break;
						case ECommonsAdamantineManagedModelFieldType.TIME:
							toSave[field.name] = commonsDateHiToDate(toSave[field.name]);
							break;
						case ECommonsAdamantineManagedModelFieldType.EMAIL:
							if (!COMMONS_REGEX_PATTERN_EMAIL.test(toSave[field.name])) throw new Error('Invalid email address');
							break;
						case ECommonsAdamantineManagedModelFieldType.URL:
							if (!COMMONS_REGEX_PATTERN_URL.test(toSave[field.name])) throw new Error('Invalid URL');
							break;
						case ECommonsAdamantineManagedModelFieldType.STRINGARRAY:
							toSave[field.name] = toSave[field.name]
									.split(',')
									.map((item: string): string => item.trim())
									.filter((item: string): boolean => item !== '');
							if (toSave[field.name].length === 0) toSave[field.name] = undefined;
							break;
					}
				} catch (e) {
					console.log(e);
					invalidField = field;
					break;
				}
			}
		}
		
		if (invalidField) {
			return {
					valid: false,
					invalidField: invalidField
			};
		}

		// For managed models, we mandate the name field so that the strong types and generics work.
		if (!commonsTypeHasPropertyString(toSave, 'name')) {
			const nameField: ICommonsManagedModelField|undefined = this.metadata.manageableFields
					.find((field: ICommonsManagedModelField): boolean => field.name === 'name');
			if (!nameField) throw new Error('No name field for this model. Is this model actually managable?');
			
			return {
					valid: false,
					invalidField: nameField
			};
		}
		
		const typecast: Omit<ICommonsManaged, 'id'> = toSave as Omit<ICommonsManaged, 'id'>;

		return {
				valid: true,
				toSave: typecast
		};
	}
	
	isFormCompleted(): boolean {
		const validityOutcome: TValidityOutcome = this.validateForm();
		
		return validityOutcome.valid;
	}
	
	doSave(): void {
		const validityOutcome: TValidityOutcome = this.validateForm();
		
		if (validityOutcome.valid === false) {	// can't use !valid as the type requires false explicity
			this.snackService.error(`Invalid data for ${validityOutcome.invalidField.description || validityOutcome.invalidField.name}`);
			return;
		}
		if (validityOutcome.valid !== true) throw new Error('ValidityOutcome.valid is not true or false.');
		
		if (!this.row) {
			this.create.emit(validityOutcome.toSave);
		} else {
			const clone: ICommonsManaged = {
					...this.row,
					...validityOutcome.toSave
			};
			this.update.emit(clone);
		}
	}
	
	doCancel(): void {
		this.cancel.emit();
	}
}

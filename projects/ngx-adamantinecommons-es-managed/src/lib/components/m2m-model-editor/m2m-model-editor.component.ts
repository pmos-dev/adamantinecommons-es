import { Component, OnInit, OnChanges, SimpleChanges } from '@angular/core';
import { Input, Output, EventEmitter } from '@angular/core';

import { commonsArrayAdded, commonsArrayRemoved } from 'tscommons-es-core';
import { ICommonsManaged } from 'tscommons-es-models-adamantine';
import { ICommonsManagedModelMetadata } from 'tscommons-es-models-adamantine';

import { CommonsComponent } from 'ngx-angularcommons-es-core';

import { ECommonsTableSelectable } from 'ngx-materialcommons-es-table';

@Component({
		selector: 'adamantine-m2m-model-editor',
		templateUrl: './m2m-model-editor.component.html',
		styleUrls: ['./m2m-model-editor.component.less']
})
export class M2MModelEditorComponent extends CommonsComponent implements OnInit, OnChanges {
	ECommonsTableSelectable = ECommonsTableSelectable;
	
	@Input() bMetadata: ICommonsManagedModelMetadata|undefined;
	@Input() allowWrite: boolean = false;
	@Input() paginate: number|undefined;

	@Input() a: ICommonsManaged|undefined;
	@Input() bs: ICommonsManaged[] = [];
	
	@Input() selecteds: ICommonsManaged[] = [];
	
	@Output() private link: EventEmitter<ICommonsManaged> = new EventEmitter<ICommonsManaged>(true);
	@Output() private unlink: EventEmitter<ICommonsManaged> = new EventEmitter<ICommonsManaged>(true);
	
	private lastSelectedIds: number[] = [];
	
	ngOnInit(): void {
		super.ngOnInit();
		
		this.load();
	}
	
	ngOnChanges(changes: SimpleChanges): void {
		this.load();
		
		if (changes.a && changes.a.currentValue !== changes.a.previousValue && changes.a.currentValue === undefined) {
			this.selecteds = [];
		}
	}
	
	private load(): void {
		this.lastSelectedIds = this.selecteds
				.map((s: ICommonsManaged): number => s.id);
	}
	
	doSelectedsChanged(): void {
		const nowSelectedIds: number[] = this.selecteds
				.map((s: ICommonsManaged): number => s.id);
		
		const addeds: number[] = commonsArrayAdded(this.lastSelectedIds, nowSelectedIds);
		const removeds: number[] = commonsArrayRemoved(this.lastSelectedIds, nowSelectedIds);

		for (const id of addeds) {
			const match: ICommonsManaged|undefined = this.bs
					.find((b: ICommonsManaged): boolean => b.id === id);
					
			if (match) this.link.emit(match);
		}

		for (const id of removeds) {
			const match: ICommonsManaged|undefined = this.bs
					.find((b: ICommonsManaged): boolean => b.id === id);
					
			if (match) this.unlink.emit(match);
		}
		
		this.lastSelectedIds = nowSelectedIds;
	}
}

import { NgModule, ModuleWithProviders } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';

import { NgxAngularCommonsEsCoreModule } from 'ngx-angularcommons-es-core';
import { NgxAngularCommonsEsAppModule } from 'ngx-angularcommons-es-app';
import { NgxAngularCommonsEsPipeModule } from 'ngx-angularcommons-es-pipe';

import { NgxMaterialCommonsEsCoreModule } from 'ngx-materialcommons-es-core';
import { NgxMaterialCommonsEsAppModule } from 'ngx-materialcommons-es-app';
import { NgxMaterialCommonsEsFormModule } from 'ngx-materialcommons-es-form';
import { NgxMaterialCommonsEsMobileModule } from 'ngx-materialcommons-es-mobile';
import { NgxMaterialCommonsEsTableModule } from 'ngx-materialcommons-es-table';

import { DurationComponent } from './components/duration/duration.component';
import { DataFieldsFormComponent } from './components/data-fields-form/data-fields-form.component';
import { ManagedModelListComponent } from './components/managed-model-list/managed-model-list.component';
import { ManagedModelFormComponent } from './components/managed-model-form/managed-model-form.component';
import { ManagedModelComponent } from './components/managed-model/managed-model.component';
import { M2MModelEditorComponent } from './components/m2m-model-editor/m2m-model-editor.component';
import { M2MModelGridEditorComponent } from './components/m2m-model-grid-editor/m2m-model-grid-editor.component';

import { AdamantineCommonsOutcomeSnackService } from './services/outcome-snack.service';

@NgModule({
		imports: [
				CommonModule,
				FormsModule,
				NgxAngularCommonsEsCoreModule,
				NgxAngularCommonsEsAppModule,
				NgxAngularCommonsEsPipeModule,
				NgxMaterialCommonsEsCoreModule,
				NgxMaterialCommonsEsAppModule,
				NgxMaterialCommonsEsFormModule,
				NgxMaterialCommonsEsMobileModule,
				NgxMaterialCommonsEsTableModule
		],
		declarations: [
				DurationComponent,
				DataFieldsFormComponent,
				ManagedModelListComponent,
				ManagedModelFormComponent,
				ManagedModelComponent,
				M2MModelEditorComponent,
				M2MModelGridEditorComponent
		],
		exports: [
				ManagedModelListComponent,
				ManagedModelFormComponent,
				ManagedModelComponent,
				M2MModelEditorComponent,
				M2MModelGridEditorComponent
		]
})
export class NgxAdamantineCommonsEsManagedModule {
	static forRoot(): ModuleWithProviders<NgxAdamantineCommonsEsManagedModule> {
		return {
				ngModule: NgxAdamantineCommonsEsManagedModule,
				providers: [
						AdamantineCommonsOutcomeSnackService
				]
		};
	}
}

import { ECommonsTableSelectable } from 'ngx-materialcommons-es-table';

export type TManagedModelCustomAction = {
		action: string;
		caption: string;
		selectable: ECommonsTableSelectable|undefined;
		requireAccessToManage: boolean;
		class?: string;
};

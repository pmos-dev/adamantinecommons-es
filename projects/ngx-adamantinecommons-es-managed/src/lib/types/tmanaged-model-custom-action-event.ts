import { ICommonsManaged } from 'tscommons-es-models-adamantine';

export type TManagedModelCustomActionEvent = {
		action: string;
		selecteds: ICommonsManaged[];
};

import { ICommonsManaged } from 'tscommons-es-models-adamantine';

export type TM2MLinkPair = {
		a: ICommonsManaged;
		b: ICommonsManaged;
};

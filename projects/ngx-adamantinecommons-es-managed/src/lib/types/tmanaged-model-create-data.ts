import { ICommonsManaged } from 'tscommons-es-models-adamantine';

export type TManagedModelCreateData = Omit<ICommonsManaged, 'id'>;

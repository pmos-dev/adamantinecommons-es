import { ICommonsFirstClass } from 'tscommons-es-models';
import { ECommonsMoveDirection } from 'tscommons-es-models';

export type TManagedModelMovement = {
		selecteds: ICommonsFirstClass[];
		direction: ECommonsMoveDirection;
};
